import React from 'react';
import { Link } from "react-router-dom";
import Popup from "reactjs-popup";
import Footer from "../footer/footer";

class SettingCameraNext extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.state = { open: false };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({ open: true });
    }
    closeModal() {
        this.setState({ open: false });
    }

    render() {
        return (

            <div className="content add-camera-setup settings-camera-page">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <form className="white-box" onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="heading"><h2>Add & Setup Camera</h2></div>
                        </div>
                        <div className="col-md-6">
                            <div className="camera-settings">
                                <Link className="" to={"/setting-camera"}>
                                    Connect
                       </Link>
                                <Link className="active" to={"/add-camera-setup"}>
                                    Camera settings
                       </Link>
                            </div>
                        </div>
                    </div>
                    <div className="main-content">
                        <div className="camera-type select-list">
                            <label>Select Model</label>
                            <ul>
                                <li><Link className="active default-btn btn">Item detection</Link></li>
                                <li><Link className="default-btn btn">Activity detection</Link></li>
                            </ul>
                        </div>

                        <div className="camera-type select-list">
                            <label>Select Model</label>
                            <ul>
                                <li><Link className="active default-btn btn">Hairnet</Link></li>
                                <li><Link className="default-btn btn">Apron</Link></li>
                                <li><Link className="active default-btn btn">Gloves</Link></li>
                                <li><Link className="default-btn btn">Gloves</Link></li>
                            </ul>
                        </div>

                        <div className="camera-type select-list">
                            <label>Add Schedule</label>
                            <ul>
                                <li className="add-new" onClick={this.openModal}> Add </li>
                            </ul>
                            <span>Every 2 days on Thu at 6:30pm</span>
                        </div>


                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-6">
                            <Link className="skip-btn btn default-btn " to={"#"}>
                                Setup to other cameras
                    </Link>
                        </div>
                        <div className="col-md-6 text-right">
                            <Link className=" default-btn btn" to={"/setting-camera"}>
                                Cancel
                    </Link>
                            <button type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Next</button>
                        </div>
                    </div>

                </form>
                <Footer />
                <Popup
                    open={this.state.open}
                    closeOnDocumentClick
                    onClose={this.closeModal}
                >
                    <div className="modal-content">
                        <span className="close" onClick={this.closeModal}>
                            &times;
                                    </span>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae magni
                                    omnis delectus nemo, maxime molestiae dolorem numquam mollitia, voluptate
                                    ea, accusamus excepturi deleniti ratione sapiente! Laudantium, aperiam
                                    doloribus. Odit, aut.
                                </div>
                </Popup>
            </div>
        );
    }
}
export default SettingCameraNext;


