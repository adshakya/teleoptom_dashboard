import React, { Component } from "react";


import { Link } from "react-router-dom";

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'password',
            score: 'null'
        }
        this.showHide = this.showHide.bind(this);
        this.passwordStrength = this.passwordStrength.bind(this);
    }

    showHide(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            type: this.state.type === 'input' ? 'password' : 'input'
        })
    }

    passwordStrength(e) {
        if (e.target.value === '') {
            this.setState({
                score: 'null'
            })
        }

    }
    render() {
        return (
            <div className="login-inner">
                <form action='/dashboard'>
                    <div className="form-group">
                        <input type="email" className="form-control" placeholder="Email" />
                    </div>

                    <div className="form-group">
                        <input type={this.state.type} className="password__input form-control" onChange={this.passwordStrength} />
                        <span className="password__show" onClick={this.showHide}>{this.state.type === 'password' ? 'Show' : 'Hide'}</span>
                        <span className="password__strength" data-score={this.state.score} />
                    </div>
                    <Link className="forgot-password" to={"#"}>
                        Forgot Password
                        </Link>
                    <button type="submit" className="btn btn-primary btn-block btn-signup">Sign In</button>

            </form>
                
            </div>
        );
    }
}
