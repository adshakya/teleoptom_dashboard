import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Header from "./components/header/header";
import Footer from "./components/footer/footer";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from "./components/login/login";
import Dashboard from "./components/dashboard/dashboard";




class App extends React.Component {
  render() {
    return (<Router>
      <div className="wrapper">
        <Header />
        <Switch>
          <Route exact path='/' component={Login} />
          <Route path="/dashboard" component={Dashboard} />
        </Switch>

        <Footer />
      </div>
    </Router>
    );
  }
}

export default App;
